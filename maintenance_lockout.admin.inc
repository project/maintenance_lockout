<?php

/**
 * @file
 * Administrative form for setting the maintenance_lockout module.
 */

/**
 * Form builder for maintenance lockout settings.
 *
 * @see system_settings_form()
 *
 */
function maintenance_lockout_settings_form($form, &$form_state) {
  $form['maintenance_lockout_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Maintenance Message'),
    '#required' => TRUE,
    '#default_value' => variable_get('maintenance_lockout_message', t('Website is currently under maintenance.')),
    '#description' => t('Message to users when maintenance lockout is enabled.')
  );
  
  $form['users'] = array(
    '#type' => 'fieldset',
    '#title' => t('Users'),
  );
  $form['users']['maintenance_lockout_user1'] = array(
    '#type' => 'markup',
    '#markup' => t('By default, user with uid 1 is allowed to login. Enter additional usernames to allow login below.')
  );
  $form['users']['maintenance_lockout_usernames'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional Usernames'),
    '#default_value' => variable_get('maintenance_lockout_usernames', ''),
    '#description' => t('Separate usernames with a comma.')
  );

  return system_settings_form($form);
}
